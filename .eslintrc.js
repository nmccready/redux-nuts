module.exports = {
  extends: ['@znemz/eslint-config-react'],
  env: {
    node: true,
    jest: true
  },
  rules: {
    'no-multi-assign': 0,
    'no-return-assign': 0,
    'no-restricted-syntax': 0
  }
};
