# redux-nuts-core

<!-- ![NPM version][npm-image]][npm-url]
[npm-image]: https://img.shields.io/npm/v/@znemz/redux-nuts-core
[npm-url]: https://www.npmjs.com/package/@znemz/redux-nuts-core -->

Objective is to reduce redux boiler plate by creating less reducers and actions for setting / retrieving nested values in a redux store.
