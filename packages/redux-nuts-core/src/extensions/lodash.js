import { set, get, merge, cloneDeep } from 'lodash';

// cloneDeeps to keep things immutable for redux

export const mergeIn = (obj, where, val) => {
  const clone = cloneDeep(obj);
  const merged = merge(get(clone, where), val);
  return set(clone, where, merged);
};

export const setIn = (obj, where, val) => {
  const clone = cloneDeep(obj);
  return set(clone, where, val);
};
