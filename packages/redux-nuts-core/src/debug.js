import debugFab from 'debug-fabulous';
import pkg from '../package.json';

const rootDebug = debugFab.spawnable(pkg.name);

export default rootDebug;
