export function isPrimitive(test) {
  return test !== Object(test);
}
