import { noop } from 'lodash';
import util from 'util';
import rootDebug from './debug';

import { isPrimitive } from './utils';

const debug = rootDebug.spawn('test:utils');

describe('utils', () => {
  describe(isPrimitive.name, () => {
    describe('primitive', () =>
      [
        null,
        undefined,
        true,
        'string',
        100,
        100.0,
        '20',
        Symbol('test')
      ].forEach((v) => {
        const ret = isPrimitive(v);
        debug(() => ({ ret, v }));
        it(util.inspect(v), () => expect(isPrimitive(v)).toBeTruthy());
      }));

    describe('not primitive', () =>
      [(noop, {}, String, Boolean)].forEach((v) => {
        it(util.inspect(v), () => expect(isPrimitive(v)).toBeFalsy());
      }));
  });
});
