import * as loExt from '../extensions/lodash';

export const MERGE_LIB = { mergeIn: loExt.mergeIn, setIn: loExt.setIn };
export const SET_FUNCTIONS = ['setIn', 'mergeIn'];
export const SET_FN = 'setIn';
