import { cloneDeep, get } from 'lodash';
import { setter } from './reducers';

const actions = {
  SETTINGS: 'SETTINGS'
};

const settingsReducer = (() => {
  const defaultState = {
    child1: {
      one: 1,
      two: 2,
      name: 'child1'
    },
    child2: {
      one: 1,
      two: 2,
      three: 3,
      name: 'child2'
    }
  };

  const initialState = cloneDeep(defaultState);

  function settings(state = initialState, { type, payload }) {
    if (type !== actions.SETTINGS) return state;
    return setter({ defaultState, state, type, payload });
  }

  return {
    defaultState,
    initialState,
    default: settings,
    settings
  };
})();

describe('reducers:settable', () => {
  describe(settingsReducer.settings.name, () => {
    it('unhandled action returns original state', () => {
      const state = {};
      expect(settingsReducer.settings(state, { type: 'junk' })).toEqual(state);
    });

    it('valid action modifies state', () => {
      const payload = false;
      const propNames = ['child1', 'one'];
      const state = settingsReducer.settings(undefined, {
        type: actions.SETTINGS,
        payload: { payload, propNames }
      });

      const set = get(state, propNames);
      expect(set).toEqual(payload);
    });

    it('fails on undefined propNames', () => {
      expect(() =>
        settingsReducer.settings(undefined, {
          type: actions.SETTINGS,
          payload: { payload: 'crap' }
        })
      ).toThrow();
    });

    it('valid action can update state anywhere', () => {
      const payload = false;
      const state = settingsReducer.settings(undefined, {
        type: actions.SETTINGS,
        payload: { payload, propNames: ['child1', 'three'] }
      });

      expect(get(state, ['child2', 'three'])).not.toEqual(payload);
      expect(get(state, ['child1', 'three'])).toEqual(payload);
    });

    describe('merge', () => {
      it('can merge valid path', () => {
        const payload = { name: 'LingLing', junk: 1 };

        const state = settingsReducer.settings(undefined, {
          type: actions.SETTINGS,
          payload: {
            payload,
            propNames: ['child1'],
            setFn: 'mergeIn'
          }
        });

        const child1 = get(state, 'child1');

        expect(child1).toEqual({
          ...settingsReducer.defaultState.child1,
          ...payload
        });
      });

      it('can merge a path that did not exist originally', () => {
        const payload = { name: 'LingLing', junk: 1 };

        const state = settingsReducer.settings(undefined, {
          type: actions.SETTINGS,
          payload: {
            payload,
            propNames: ['child1', 'three'],
            setFn: 'mergeIn'
          }
        });

        const { three } = get(state, 'child1');
        expect(three).toEqual(payload);
      });
    });
  });
});
