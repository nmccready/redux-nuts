import { each } from 'lodash';
import { settable } from './actionCreators';

const actions = {
  JUNK: 'JUNK',
  APPLE: 'APPLE'
};
const actionCreators = {
  junk: settable(actions.JUNK),
  apple: settable(actions.APPLE)
};

describe('actionCreators', () => {
  each(actionCreators, (fn, k) => {
    it(k, () => {
      const propNames = ['propName'];
      const pay = 'somePayload';
      const { type, payload } = fn({ propNames, payload: pay });
      expect(type).toEqual(actions[k.toUpperCase()]);
      expect(payload).toEqual({
        payload: pay,
        propNames,
        setFn: 'setIn'
      });
    });
  });
});
