import { omit, get } from 'lodash';

import { isPrimitive } from '../utils';
import { MERGE_LIB, SET_FUNCTIONS, SET_FN } from './defaults';

export const reducersFactory = (
  mergeLib = MERGE_LIB,
  setFunctions = SET_FUNCTIONS
) => {
  /**
   * @param  {Object} options
   * @param  {string} options.reducerName logging / error string for easier traceability
   * @param  {Object|Immutable} options.state
   * @param  {Object} options.defaultState **NOT IMMUTABLE**
   * @param  {string} options.type
   * @param  {Any} options.payload
   *
   * Goal is to reduce the number of actions/actionCreators required to
   * actually set and update state.
   *
   * REDUCE , redux awful boilerplate.. is that a bad thing?
   *
   * Simple immutable.setIn where propName is an array . However, the propName is only allowed to be
   * set if the root field exists within the defaultState.
   *
   * This is attempt at protecting cross state setting from other reducers with the wrong action.
   *
   *
   * Example: way to set state from a valid incoming action.
   *
   * action: {
   *   type: 'PREFERENCE',
   *   payload: {
   *     payload: {} | 'some val',
   *     propNames ['bullseye', 'color'],
   *     setFn: 'setIn' | 'merge'
   *   }
   * }
   */
  const setter = ({ state, defaultState = {}, type, payload, omits = [] }) => {
    const { payload: _val, propNames, setFn } = payload || {};
    let val = _val;
    if (!isPrimitive(val)) {
      val = omit(_val, omits);
    }

    requiredSetterOpts({ propNames, defaultState, type, setFn });

    if (setFn === 'mergeIn') {
      return mergeLib.mergeIn(state, propNames, val);
    }

    return mergeLib.setIn(state, propNames, val);
  };

  return { setter };

  /*
    Basic error handling for required vars and assumptions.
  */
  function requiredSetterOpts({
    type,
    propNames,
    defaultState,
    setFn = SET_FN
  }) {
    if (setFunctions.indexOf(setFn) === -1) {
      throw new Error(
        `${type} setFn ${setFn}: is not in the allowed list of functions ${setFunctions.join(
          ' '
        )}`
      );
    }
    if (!propNames) {
      throw new Error(
        `${type} propNames: ${propNames} not defined from creator`
      );
    }

    if (!Array.isArray(propNames) || !propNames.length) {
      throw new Error(
        `${type} propNames: ${propNames} must be an Array with at least one element from creator`
      );
    }

    const [root] = propNames;

    if (!root) {
      throw new Error(`${type} root: ${root} not defined from creator`);
    }

    if (!defaultState[root]) {
      throw new Error(
        `${type} root: ${root}, does not exist in original default reducer state`
      );
    }
    return root;
  }
};

const lodashReducer = reducersFactory();

export const { setter } = lodashReducer;

export default lodashReducer;
