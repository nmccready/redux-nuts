import { SET_FN } from './defaults';

// * TODO debug-fab needs to handle async exports
// https://github.com/nmccready/debug-fabulous/issues/11
// const rootDebug = require('../debug');

// const debug = rootDebug.spawn('redux:helpers:actionCreators');

export function basic(type, payload = {}) {
  return { type, payload };
}

/**
 * @param  {Object} options
 * @param  {Array<String>} options.propNames setIn or merge field list to be set
 * @param  {Any} options.payload new value to over write
 * @param  {string} options.setFn 'setIn' or 'merge'
 */
export const settableFact = (defaultSetterFn = SET_FN) => (type, propNames) => (
  { propNames: innerPropNames, payload, setFn = defaultSetterFn } = {} // {
) =>
  // debug.spawn(`settable:${type}:${propNames.join(':')}:${setFn}`)(
  // () => payload
  // );
  basic(type, { propNames: innerPropNames || propNames, payload, setFn });
// };

export const settable = settableFact();

export default settable;
