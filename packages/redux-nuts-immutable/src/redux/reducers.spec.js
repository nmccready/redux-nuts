import { fromJS } from 'immutable';
import { setter } from './reducers';

const actions = {
  SETTINGS: 'SETTINGS'
};

const settingsReducer = (() => {
  const defaultState = {
    child1: {
      one: 1,
      two: 2,
      name: 'child1'
    },
    child2: {
      one: 1,
      two: 2,
      three: 3,
      name: 'child2'
    }
  };

  const initialState = fromJS(defaultState);

  function settings(state = initialState, { type, payload }) {
    if (type !== actions.SETTINGS) return state;
    return setter({ defaultState, state, type, payload });
  }

  return {
    defaultState,
    initialState,
    default: settings,
    settings
  };
})();

describe('immutable:reducers:settable', () => {
  describe(settingsReducer.settings.name, () => {
    it('unhandled action returns original state', () => {
      const state = {};
      expect(settingsReducer.settings(state, { type: 'junk' })).toEqual(state);
    });

    it('valid action modifies state', () => {
      const payload = false;
      const propNames = ['child1', 'one'];
      const state = settingsReducer.settings(undefined, {
        type: actions.SETTINGS,
        payload: { payload, propNames }
      });

      const set = state.getIn(propNames);
      expect(set).toEqual(payload);
    });

    it('fails on undefined propNames', () => {
      expect(() =>
        settingsReducer.settings(undefined, {
          type: actions.SETTINGS,
          payload: { payload: 'crap' }
        })
      ).toThrow();
    });

    it('valid action does not modify a different actions state', () => {
      const payload = false;
      const state = settingsReducer.settings(undefined, {
        type: actions.SETTINGS,
        payload: { payload, propNames: ['child1', 'three'] }
      });

      expect(state.getIn(['child2', 'three'])).not.toEqual(payload);
      expect(state.getIn(['child1', 'three'])).not.toEqual(payload);
    });

    describe('merge', () => {
      it('can merge valid path', () => {
        const payload = { name: 'LingLing', junk: 1 };

        const state = settingsReducer.settings(undefined, {
          type: actions.SETTINGS,
          payload: {
            payload,
            propNames: ['child1'],
            setFn: 'mergeIn'
          }
        });

        const child1 = state.get('child1').toJS();
        expect(child1).toEqual({
          ...settingsReducer.defaultState.child1,
          ...payload
        });
      });

      it("can't merge invalid path", () => {
        const payload = { name: 'LingLing', junk: 1 };

        const state = settingsReducer.settings(undefined, {
          type: actions.SETTINGS,
          payload: {
            payload,
            propNames: ['child1', 'three'],
            setFn: 'mergeIn'
          }
        });

        const { three } = state.get('child1').toJS();
        expect(three).not.toEqual(payload);
      });
    });
  });
});
