import { reducers } from '@znemz/redux-nuts-core';
import * as MERGE_LIB from '../extensions/Immutable';

const immutableReducer = reducers.reducersFactory(MERGE_LIB);

export const { setter } = immutableReducer;

export default immutableReducer;
