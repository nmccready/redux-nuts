export function toJS(obj) {
  return JSON.parse(JSON.stringify(obj));
}

// eslint-disable-next-line import/prefer-default-export
export const getSize = (maybeImmutable) =>
  maybeImmutable && maybeImmutable.size ? maybeImmutable.size : 0;

export const mergeIn = (immut, where, val) => {
  const merged = immut.getIn(where).merge(val);
  return immut.setIn(where, merged);
};

export const getJS = (maybeImmut) =>
  maybeImmut.toJS ? maybeImmut.toJS() : maybeImmut;

export const setIn = (immut, where, val) => immut.setIn(where, val);
