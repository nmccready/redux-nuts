# redux-nuts

<!-- [![build status][gitlab-ci-image]] -->

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)

## Test

To run a specific project run, `yarn scope -- redux-nuts-core -- jest` or `yarn scope -- redux-nuts* -- jest`.

<!-- [gitlab-ci-image]: https://gitlab.com/nmccready/gitlab-cdredux-nuts/badges/master/build.svg -->
