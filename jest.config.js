module.exports = {
  roots: ['<rootDir>'],
  setupTestFrameworkScriptFile: 'jest-extended',
  testEnvironment: 'node',
  collectCoverageFrom: ['**/src/**/*.{js,jsx}'],
  coverageReporters: ['text', 'json', 'json-summary', 'lcov'],
  coverageThreshold: {
    global: {
      branches: 0.01,
      functions: 0.01,
      lines: 0.01,
      statements: 0.01
    }
  },
  coverageDirectory: 'coverage/latest'
};
